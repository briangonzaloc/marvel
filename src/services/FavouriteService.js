export const driver = window.localStorage

export const getFavourites = () => {
	return JSON.parse(driver.getItem('favourites'))
}

export const setFavorutes = data => {
	driver.setItem('favourites', JSON.stringify(data))
}

export const destroyFavourites = () => {
	driver.removeItem('favourites')
}
