import React from 'react'
import styled from 'styled-components'
import Device from '../commons/Device'
import PropTypes from 'prop-types'

const Styles = styled.svg`
	width: ${props => (props.size === 'small' ? '14px' : '24px')};
	height: ${props => (props.size === 'small' ? '14px' : '24px')};
	fill: transparent;
	stroke: ${props => props.color || props.theme.font.colors.dark};

	&:hover {
		cursor: pointer;
		fill: ${props => props.color || props.theme.font.colors.dark};
	}

	@media ${Device.xs} {
		width: 14px;
		height: 14px;
	}
`

const Star = ({color, size, onClick}) => {
	return (
		<Styles
			onClick={onClick}
			color={color}
			size={size}
			xmlns="http://www.w3.org/2000/Styles"
			viewBox="0 0 24 24"
		>
			<path d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z" />
		</Styles>
	)
}

export default Star

Star.propTypes = {
	color: PropTypes.string,
	size: PropTypes.string,
	onClick: PropTypes.func
}
