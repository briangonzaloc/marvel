import React from 'react'
import {shallow} from 'enzyme'
import Star from './Star'

const props = {
	color: 'blue',
	size: '14px',
	onClick: jest.fn()
}

describe('Star component', () => {
	const component = shallow(
		<Star color={props.color} size={props.size} onClick={props.onClick} />
	)

	it('click should execute function', () => {
		component.simulate('click')
		expect(props.onClick).toHaveBeenCalled()
	})

	expect(component).toMatchSnapshot()
})
