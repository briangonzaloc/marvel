/*----- Core -----*/
import React, {useContext} from 'react'

/*----- Components -----*/
import Styles from './style'
import List from '../../components/lists/comicCharactersList/index'
import ComicDetail from '../ComicDetail/index'

/*----- Contexts -----*/
import {ComicDetailContext} from '../../contexts/comicDetail/index'

const Home = () => {
	const {comicDetail} = useContext(ComicDetailContext)

	return (
		<Styles>
			{comicDetail.data && comicDetail.data.id ? <ComicDetail /> : <List />}
		</Styles>
	)
}

export default Home
