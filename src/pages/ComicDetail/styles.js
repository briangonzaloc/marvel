import styled from 'styled-components'
import Device from '../../commons/Device'

const Styles = styled.section`
	display: flex;
	padding: 5% 3%;

	.container-image {
		width: 45%;
		background-position: top right;
		background-size: contain;
		background-repeat: no-repeat;
		min-height: 75vh;
	}

	.container-data {
		width: 45%;
		font-family: ${props => props.theme.font.family};
		padding: 0px 30px;
		h1 {
			margin-top: 0px;
			font-size: ${props => props.theme.font.sizes.lg};
			color: ${props => props.theme.font.colors.dark};
		}

		.creator-data {
			margin: 40px 0px;
			color: ${props => props.theme.font.colors.light};
			font-weight: 600;
			font-size: ${props => props.theme.font.sizes.md};
			p {
				margin-bottom: 0px;
				margin-top: 0px;
			}
		}

		.description-data {
			color: ${props => props.theme.font.colors.light};
			font-size: ${props => props.theme.font.sizes.md};
		}
	}

	@media ${Device.xs} {
		padding-top: 50px;
		.container-data {
			width: 50%;
			padding: 0px 10px;

			h1 {
				font-size: ${props => props.theme.font.sizes.sm};
			}

			.creator-data {
				margin: 10px 0px;
				font-size: ${props => props.theme.font.sizes.xs};
			}

			.description-data {
				font-size: ${props => props.theme.font.sizes.xs};
			}
		}
	}
`

export default Styles
