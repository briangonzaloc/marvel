/*----- Core -----*/
import React, {useContext} from 'react'
import Moment from 'moment'

/*----- Contexts -----*/
import {ComicDetailContext} from '../../contexts/comicDetail/index'

/*----- Components -----*/
import Styles from './styles'

const ComicDetail = () => {
	const {
		comicDetail: {data}
	} = useContext(ComicDetailContext)

	const getCreator = role => {
		let creators = data.creators.items.filter(creator => {
			return creator.role.includes(role)
		})
		creators = creators.map(creator => creator.name)
		return creators.join(', ')
	}

	return (
		<Styles>
			<div
				className="container-image"
				style={{
					backgroundImage: `url(${data.thumbnail.path}/portrait_incredible.${data.thumbnail.extension}`
				}}
			/>
			<div className="container-data">
				<h1>{data.title}</h1>
				<div className="creator-data">
					<p>Published: {Moment(data.dates[0].date).format('ll')}</p>
					<p>Writer: {getCreator('writer')}</p>
					<p>Penciler: {getCreator('penciler')}</p>
					<p>Cover Artist: {getCreator('cover')}</p>
				</div>
				<p className="description-data">{data.description}</p>
			</div>
		</Styles>
	)
}

export default ComicDetail
