import {createGlobalStyle} from 'styled-components'

export default createGlobalStyle`
    body{
        background-color:${props => props.theme.background};
    }
    .center{
        display: flex;
        justify-content: center;
        align-items: center;
    }
`
