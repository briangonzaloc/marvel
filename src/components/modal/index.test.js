import React from 'react'
import {shallow} from 'enzyme'
import Modal from './index'

const props = {
	visible: false,
	setVisible: jest.fn(),
	title: 'Title',
	onEndScroll: jest.fn()
}

describe('<ComicList>', () => {
	const component = shallow(
		<Modal
			title={props.title}
			onEndScroll={props.onEndScroll}
			visible={props.visible}
			setVisible={props.setVisible}
		/>
	)

	it('should render title correctly', () => {
		expect(component.find('h2').text()).toEqual(props.title)
	})

	it('should execute set visible', () => {
		const close = component.find('.button-close')
		close.simulate('click')
		expect(props.setVisible).toHaveBeenCalled()
	})

	expect(component).toMatchSnapshot()
})
