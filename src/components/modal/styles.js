import styled, {css} from 'styled-components'
import Device from '../../commons/Device'

const Styles = styled.div`
    background: #00000075;
    opacity:1;
    position: fixed;
    top: 0;
    left: 0;
    z-index:1300;
    height: 100%;
    width: 100%;
    transition: all .5s;
    cursor:initial;

    .modal-content{
        border-radius: 12px;
        background-color:#fff;
        position:relative;
        padding:0;
        top:12vh;
        

        .button-close{
            cursor:pointer;
            position:absolute;
            right:20px;
            top:10px;
            color: black;
            padding:5px;
            font-size:2em;
            color: ${props => props.theme.font.colors.light};
        }

        h2{
            padding:20px;
            font-family: ${props => props.theme.font.family};
            color: ${props => props.theme.font.colors.dark};
        }

        .modal-body{
            width:50vh;
            height:60vh;
            overflow:overlay;
            position:relative;
            padding: 20px;
            

            ::-webkit-scrollbar {
                width: 8px;
            }

            ::-webkit-scrollbar-track {
                background: #f1f1f1; 
            }

            ::-webkit-scrollbar-thumb {
                background: #888; 
            }

            ::-webkit-scrollbar-thumb:hover {
                background: #555; 
            }
        }

    }


    ${props =>
			!props.visible &&
			css`
				display: none;
			`}

    @media ${Device.xs} {
        .modal-content{
            top:12vh;
            
            .modal-body{
                width:45vh;
            }

        }

        h2{
            font-size: ${props => props.theme.font.sizes.md}
        }

    }
`

export default Styles
