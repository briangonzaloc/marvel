/*----- Core -----*/
import React from 'react'
import PropTypes from 'prop-types'

/*----- Components -----*/
import Styles from './styles'

const Modal = ({visible, setVisible, title, children, onEndScroll}) => {
	const handlerEndScroll = () => {
		if (onEndScroll) {
			onEndScroll()
		}
	}

	return (
		<Styles visible={visible}>
			<div className="center">
				<div className="modal-content">
					<span
						className="button-close"
						onClick={() => {
							setVisible(false)
						}}
					>
						&times;
					</span>
					<h2>{title}</h2>
					<div
						className="modal-body"
						onScroll={event => {
							const target = event.target
							if (
								target.scrollHeight <=
								target.scrollTop + target.clientHeight
							) {
								handlerEndScroll()
							}
						}}
					>
						{children}
					</div>
				</div>
			</div>
		</Styles>
	)
}

export default Modal

Modal.propTypes = {
	visible: PropTypes.bool,
	setVisible: PropTypes.func,
	title: PropTypes.string,
	children: PropTypes.shape({}),
	onEndScroll: PropTypes.func
}
