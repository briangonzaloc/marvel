import styled from 'styled-components'

const Styles = styled.div`
	display: inline-block;
	box-sizing: border-box;
	position: relative;
	margin: 10px;
	width: 256px;

	&:hover {
		cursor: pointer;
	}

	.card-title {
		position: absolute;
		bottom: 0px;
		margin: 16px;
		font-family: ${props => props.theme.comicCharacter.font.family};
		font-weight: ${props => props.theme.comicCharacter.font.weight};
		font-size: ${props => props.theme.comicCharacter.font.size};
		color: ${props => props.theme.comicCharacter.font.color};
	}

	.card-image {
		background-position: center center;
		background-size: contain;
		background-repeat: no-repeat;
		min-height: 380px;
	}

	.container-star {
		position: absolute;
		right: 0;
		top: 10px;
		padding-right: 10px;
	}
`

export default Styles
