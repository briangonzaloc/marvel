/*----- Core -----*/
import React, {useState, useContext} from 'react'
import PropTypes from 'prop-types'
import {
	SET_COMICS,
	RESET_COMICS_DATA,
	RESET_COMICS
} from '../../../constants/actionTypes'

/*----- Components -----*/
import Styles from './styles'
import Modal from '../../modal/index'
import ComicList from '../../lists/comicsList/index'
import Star from '../../../icons/Star'

/*----- Contexts -----*/
import {ComicsContext} from '../../../contexts/comics/index'

/*----- API -----*/
import {fetchComics} from '../../../api/ManagerApi'

const Card = ({imageUrl, name, id}) => {
	const [comicsVisible, setComicsVisible] = useState(false)
	const {comics, dispatch} = useContext(ComicsContext)

	const getComics = async (params = {}) => {
		let result = await fetchComics(id, params)
		if (result.status === 200) {
			dispatch({type: SET_COMICS, payload: result.data.data})
		}
	}

	const getSelectedComics = async () => {
		for (let name of comics.names) {
			await getComics({titleStartsWith: name})
		}
	}

	const handlerClick = () => {
		setComicsVisible(true)

		if (!comics.names.length) {
			dispatch({type: RESET_COMICS})
			getComics({orderBy: 'onsaleDate'})
		} else {
			dispatch({type: RESET_COMICS_DATA})
			getSelectedComics()
		}
	}

	const onEndScroll = () => {
		if (comics.count < comics.total) {
			getComics({
				offset: `${comics.offset + comics.limit}`,
				orderBy: 'onsaleDate'
			})
		}
	}

	return (
		<Styles>
			<div
				className="card-image"
				style={{
					backgroundImage: `url(${imageUrl})`
				}}
				onClick={handlerClick}
			>
				<span className="container-star">
					<Star color={'#FFFFFF'} />
				</span>
				<p className="card-title">{name}</p>
			</div>
			<Modal
				visible={comicsVisible}
				setVisible={setComicsVisible}
				title={name}
				onEndScroll={onEndScroll}
			>
				<ComicList />
			</Modal>
		</Styles>
	)
}

export default Card

Card.propTypes = {
	imageUrl: PropTypes.string,
	name: PropTypes.string,
	id: PropTypes.string
}
