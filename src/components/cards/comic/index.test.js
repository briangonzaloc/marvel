import React from 'react'
import {shallow} from 'enzyme'

import Card from './index'

const props = {
	title: 'Avengers West Coast Annual (1986) #8',
	description:
		'Introducing Raptor! But will he join the Avengers West Coast or become one of their nemeses? Plus, Ultron s mechanized evil rears his ugly face.',
	imageUrl:
		'http://i.annihil.us/u/prod/marvel/i/mg/c/03/57d318953805a/standard_medium.jpg'
}

describe('Comic Card', () => {
	const component = shallow(
		<Card
			title={props.title}
			description={props.description}
			imageUrl={props.imageUrl}
		/>
	)

	it('should render title correctly', () => {
		expect(component.find('h4').text()).toEqual(props.title)
	})

	it('should render description correctly', () => {
		expect(component.find('p').text()).toEqual(props.description)
	})

	it('should render image correctly', () => {
		expect(component.find('div.card-image').get(0).props.style).toHaveProperty(
			'backgroundImage',
			`url(${props.imageUrl})`
		)
	})

	expect(component).toMatchSnapshot()
})
