/*----- Core -----*/
import React from 'react'
import PropTypes from 'prop-types'
import Star from '../../../icons/Star'

/*----- Components -----*/
import Styles from './styles'

const ComicCard = ({title, description, imageUrl}) => {
	return (
		<Styles>
			<div
				className="card-image"
				style={{
					backgroundImage: `url(${imageUrl})`
				}}
			/>
			<div className="card-data">
				<div className="title-container">
					<h4>{title}</h4>
					<Star size={'small'} />
				</div>

				<p>{description}</p>
			</div>
		</Styles>
	)
}

export default ComicCard

ComicCard.propTypes = {
	title: PropTypes.string,
	imageUrl: PropTypes.string,
	description: PropTypes.string
}
