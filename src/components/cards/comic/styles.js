import styled from 'styled-components'
import Device from '../../../commons/Device'

const Styles = styled.div`
	display: flex;
	min-height: 100px;
	padding-bottom: 10px;

	.card-image {
		width: 25%;
		background-position: center top;
		background-size: contain;
		background-repeat: no-repeat;
		min-height: 100px;
	}
	.card-data {
		width: 70%;
		padding-left: 10px;
		font-family: ${props => props.theme.font.family};

		.title-container {
			display: flex;
		}

		h4 {
			margin-top: 0px;
			margin-bottom: 8px;
			color: ${props => props.theme.font.colors.dark};
			font-weight: bold;
			padding-right: 5px;
		}

		p {
			margin-top: 0px;
			color: ${props => props.theme.font.colors.light};
		}
	}

	@media ${Device.xs} {
		.card-data {
			h4 {
				font-size: ${props => props.theme.font.sizes.sm};
			}
			p {
				font-size: ${props => props.theme.font.sizes.xs};
			}
		}
	}
`

export default Styles
