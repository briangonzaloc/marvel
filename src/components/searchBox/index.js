/*----- Core -----*/
import React, {useState, useEffect, useContext, useCallback} from 'react'
import {withRouter} from 'react-router-dom'
import queryString from 'query-string'
import PropTypes from 'prop-types'
import {
	SET_COMIC_CHARACTERS,
	SET_COMIC_DETAIL,
	RESET_COMIC_CHARACTERS,
	SET_COMIC_NAMES,
	RESET_COMICS
} from '../../constants/actionTypes'

/*----- API -----*/
import {getComicDetail, fetchComicCharacters} from '../../api/ManagerApi'

/*----- Contexts -----*/
import {ComicCharactersContext} from '../../contexts/comicCharacters/index'
import {ComicDetailContext} from '../../contexts/comicDetail/index'
import {ComicsContext} from '../../contexts/comics/index'

/*----- Components -----*/
import Styles from './styles'
import SearchIcon from '../../icons/Search'

const SearchBox = ({history}) => {
	const [search, setSearch] = useState('')
	const {dispatch} = useContext(ComicCharactersContext)
	const {dispatchDetail} = useContext(ComicDetailContext)
	const {dispatch: dispatchComic} = useContext(ComicsContext)
	const [firstTime, setFirstTime] = useState(true)

	const fetchCharacters = useCallback(
		async params => {
			let result = await fetchComicCharacters(params)
			let searched = params.nameStartsWith ? {...params} : {}
			if (result.status === 200) {
				dispatch({
					type: SET_COMIC_CHARACTERS,
					payload: {...result.data.data, searched}
				})
			}
		},
		[dispatch]
	)

	const fetchComicDetail = async comicId => {
		let result = await getComicDetail(comicId)
		if (result.status === 200) {
			dispatchDetail({
				type: SET_COMIC_DETAIL,
				payload: result.data.data.results[0]
			})
		} else {
			dispatchDetail({type: SET_COMIC_DETAIL, payload: {}})
		}
	}

	const getRandomInt = () => {
		let totalChars = 1493
		let min = 0
		return Math.floor(Math.random() * (totalChars - min + 1)) + min
	}

	const resetData = useCallback(() => {
		dispatchDetail({
			type: SET_COMIC_DETAIL,
			payload: {}
		})
		dispatch({type: RESET_COMIC_CHARACTERS})

		dispatchComic({type: RESET_COMICS})
	}, [dispatchDetail, dispatch, dispatchComic])

	const searchData = name => {
		resetData()
		if (name && name.length) {
			if (isMarvelLink(name)) {
				let url = name.split('/')
				let comicId = url[url.length - 2]
				fetchComicDetail(comicId)
			} else {
				fetchCharacters({nameStartsWith: name})
			}
		}
	}

	useEffect(() => {
		const parseURI = async () => {
			resetData()

			let params = queryString.parse(history.location.search)
			let comic = params.comic
			let character = params.character
			setFirstTime(false)

			if (!character) {
				fetchCharacters({
					limit: 1,
					offset: getRandomInt()
				})
				return
			}

			if (typeof character === 'string') {
				fetchCharacters({name: character})
			}
			if (typeof character === 'object') {
				for (let name of character) {
					fetchCharacters({name: name})
				}
			}
			if (comic) {
				if (typeof comic === 'string') {
					dispatchComic({type: SET_COMIC_NAMES, payload: [comic]})
				}
				if (typeof comic === 'object') {
					dispatchComic({type: SET_COMIC_NAMES, payload: comic})
				}
			}
		}

		if (firstTime) {
			parseURI()
		}
	}, [
		history.location.search,
		firstTime,
		fetchCharacters,
		resetData,
		dispatchComic
	])

	const isMarvelLink = link => {
		return link.includes('https://www.marvel.com/comics/issue/')
	}

	return (
		<Styles>
			<span>
				<i className="center icon">
					<SearchIcon />
				</i>
				<input
					value={search}
					placeholder="Buscar"
					onChange={e => {
						setSearch(e.target.value)
					}}
					onKeyPress={e => {
						if (e.key === 'Enter') {
							searchData(search)
						}
					}}
				/>
			</span>
		</Styles>
	)
}

export default withRouter(SearchBox)

SearchBox.propTypes = {
	history: PropTypes.shape({
		location: PropTypes.shape({
			search: PropTypes.string
		})
	})
}
