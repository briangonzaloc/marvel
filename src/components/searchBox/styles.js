import styled from 'styled-components'
import Device from '../../commons/Device'

const Styles = styled.div`
	width: 100%;
	height: 80%;
	span {
		display: flex;
		height: 100%;
	}
	input {
		width: 100%;
		padding-left: 10px;
		font-size: ${props => props.theme.searchBox.font.sizes.lg};
		color: ${props => props.theme.searchBox.font.color};
		font-family: ${props => props.theme.searchBox.font.family};
		border: 0px;
	}

	input:focus {
		outline: none;
	}

	.icon {
		svg {
			fill: #dbdbdb;
			width: 1.5em;
			height: 1.5em;
			display: inline-block;
		}
	}

	@media ${Device.xs} {
		input {
			min-height: 20px;
			font-size: ${props => props.theme.searchBox.font.sizes.xs};
		}

		.icon {
			svg {
				width: 1em;
				height: 1em;
			}
		}
	}
`

export default Styles
