import React from 'react'
import {shallow} from 'enzyme'
import List from './index'
import {ComicsContext} from '../../../contexts/comics/index'

const comics = {
	results: [
		{
			title: 'title',
			description: 'description',
			imageUrl: 'url'
		},
		{
			title: 'title B',
			description: 'description B',
			imageUrl: 'url B'
		}
	]
}

describe('<ComicList>', () => {
	const fnc = jest.fn

	const component = shallow(
		<ComicsContext.Provider value={{comics, fnc}}>
			<List />
		</ComicsContext.Provider>
	)

	it('context should be defined', () => {
		// expect(component.find(<ComicCard/>).length).toEqual(2);
		expect(component).toMatchSnapshot()
	})
})
