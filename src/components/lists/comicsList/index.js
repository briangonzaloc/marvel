/*----- Core -----*/
import React, {useContext} from 'react'

/*----- Contexts -----*/
import {ComicsContext} from '../../../contexts/comics/index'

/*----- Components -----*/
import ComicCard from '../../cards/comic/index'

const List = () => {
	const {comics} = useContext(ComicsContext)

	const getCards = () => {
		return comics.results.map(comic => (
			<ComicCard
				key={comic.id}
				title={comic.title}
				description={comic.description}
				imageUrl={`${comic.thumbnail.path}/standard_medium.${comic.thumbnail.extension}`}
			/>
		))
	}

	if (!comics) {
		return ''
	}

	return <div>{getCards()}</div>
}

export default List
