import styled from 'styled-components'
import Device from '../../../commons/Device'

const Styles = styled.div`
	display: flex;
	justify-content: center;
	align-items: stretch;
	flex-flow: row wrap;
	padding: 5% 3%;

	@media ${Device.xs} {
		padding: 50px 10px;
	}
`

export default Styles
