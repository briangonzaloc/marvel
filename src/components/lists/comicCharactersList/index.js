/*----- Core -----*/
import React, {useContext, useEffect, useRef} from 'react'

/*----- Contexts -----*/
import {ComicCharactersContext} from '../../../contexts/comicCharacters/index'

/*----- Components -----*/
import Card from '../../cards/comicCharacter/index'
import Styles from './styles'

import {SET_COMIC_CHARACTERS} from '../../../constants/actionTypes'

/*----- API -----*/
import {fetchComicCharacters} from '../../../api/ManagerApi'

const List = () => {
	const {comicCharacters, dispatch} = useContext(ComicCharactersContext)
	const listRef = useRef()

	useEffect(() => {
		const onScroll = async () => {
			let isBottom =
				listRef.current.getBoundingClientRect().bottom <= window.innerHeight
			if (isBottom) {
				if (comicCharacters.count < comicCharacters.total) {
					let result = await fetchComicCharacters({
						offset: `${comicCharacters.offset + comicCharacters.limit}`,
						...comicCharacters.searched
					})
					if (result.status === 200) {
						dispatch({type: SET_COMIC_CHARACTERS, payload: result.data.data})
					}
				}
			}
		}
		document.addEventListener('scroll', onScroll)

		return () => {
			document.removeEventListener('scroll', onScroll)
		}
	})

	const getCards = () => {
		return comicCharacters.results.map(character => (
			<Card
				key={character.id}
				imageUrl={`${character.thumbnail.path}/portrait_fantastic.${character.thumbnail.extension}`}
				name={character.name}
				id={character.id}
			/>
		))
	}

	if (!comicCharacters) {
		return ''
	}

	return <Styles ref={listRef}>{getCards()}</Styles>
}

export default List
