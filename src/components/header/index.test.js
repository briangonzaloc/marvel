import React from 'react'
import {shallow} from 'enzyme'

import Header from './index'
import MarvelIcon from '../../icons/Marvel'
import SearchBox from '../searchBox/index'

describe('<Header>', () => {
	const component = shallow(<Header />)

	it('should contain Marvel icon', () => {
		expect(component.contains(<MarvelIcon />)).toEqual(true)
	})

	it('Should contain Seach box component', () => {
		expect(component.contains(<SearchBox />)).toEqual(true)
	})

	expect(component).toMatchSnapshot()
})
