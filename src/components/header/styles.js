import styled from 'styled-components'

const Styles = styled.div`
	display: flex;
	flex-flow: row wrap;
	min-height: 38px;
	border-bottom: 3px ${props => props.theme.header.colors.border} solid;
	background-color: ${props => props.theme.header.colors.background};
	position: fixed;
	z-index: 1000;
	width: 100%;
	top: 0;
	.icon-container {
		width: 10%;
		svg {
			width: 80%;
			height: 60%;
		}
	}

	.search-box-container {
		width: 75%;
	}
`

export default Styles
