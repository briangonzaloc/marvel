/*----- Core -----*/
import React, {useContext} from 'react'

/*----- Components -----*/
import Styles from './styles'
import SearchBox from '../searchBox/index'
import MarvelIcon from '../../icons/Marvel'
import StarIcon from '../../icons/Star'

/*----- Contexts -----*/
import {ComicCharactersContext} from '../../contexts/comicCharacters/index'

/*----- Services -----*/
import {setFavorutes} from '../../services/FavouriteService'

const Header = () => {
	const {comicCharacters} = useContext(ComicCharactersContext)

	return (
		<Styles>
			<div className="center icon-container">
				<MarvelIcon />
			</div>
			<div className="center search-box-container">
				<SearchBox />
			</div>
			<div className="center favourite-container">
				<StarIcon
					onClick={event => {
						event.preventDefault()
						setFavorutes(comicCharacters.results)
					}}
				/>
			</div>
		</Styles>
	)
}

export default Header
