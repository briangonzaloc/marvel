/*----- Core -----*/
import React, {useReducer, createContext} from 'react'
import PropTypes from 'prop-types'

/*----- Contexts -----*/
import {initialState, comicDetailReducer} from './reducer'
export const ComicDetailContext = createContext([])

const ComicDetailProvider = ({children}) => {
	const [comicDetail, dispatchDetail] = useReducer(
		comicDetailReducer,
		initialState
	)
	return (
		<ComicDetailContext.Provider value={{comicDetail, dispatchDetail}}>
			{children}
		</ComicDetailContext.Provider>
	)
}

export default ComicDetailProvider

ComicDetailProvider.propTypes = {
	children: PropTypes.node.isRequired
}
