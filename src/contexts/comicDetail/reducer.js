import {SET_COMIC_DETAIL} from '../../constants/actionTypes'

export const initialState = {
	data: {}
}

export const comicDetailReducer = (state, action) => {
	switch (action.type) {
		case SET_COMIC_DETAIL:
			return {
				...state,
				data: action.payload
			}
		default:
			return state
	}
}
