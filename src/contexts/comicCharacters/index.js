/*----- Core -----*/
import React, {useReducer, createContext} from 'react'
import PropTypes from 'prop-types'

/*----- Contexts -----*/
import {initialState, comicCharactersReducer} from './reducer'
export const ComicCharactersContext = createContext([])

const ComicCharactersProvider = ({children}) => {
	const [comicCharacters, dispatch] = useReducer(
		comicCharactersReducer,
		initialState
	)
	return (
		<ComicCharactersContext.Provider value={{comicCharacters, dispatch}}>
			{children}
		</ComicCharactersContext.Provider>
	)
}

export default ComicCharactersProvider

ComicCharactersProvider.propTypes = {
	children: PropTypes.node.isRequired
}
