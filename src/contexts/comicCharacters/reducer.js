import {
	SET_COMIC_CHARACTERS,
	RESET_COMIC_CHARACTERS
} from '../../constants/actionTypes'

export const initialState = {
	offset: 0,
	limit: 0,
	total: 0,
	count: 0,
	results: [],
	searched: ''
}

export const comicCharactersReducer = (state, action) => {
	switch (action.type) {
		case SET_COMIC_CHARACTERS:
			return {
				...state,
				results: [...state.results, ...action.payload.results],
				offset: action.payload.offset,
				limit: action.payload.limit,
				total: action.payload.total,
				count: state.count + action.payload.count,
				searched: action.payload.searched
			}
		case RESET_COMIC_CHARACTERS:
			return {
				...state,
				...initialState
			}
		default:
			return state
	}
}
