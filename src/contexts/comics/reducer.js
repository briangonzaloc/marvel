import {
	SET_COMICS,
	RESET_COMICS,
	SET_COMIC_NAMES,
	RESET_COMICS_DATA
} from '../../constants/actionTypes'

export const initialState = {
	offset: 0,
	limit: 0,
	total: 0,
	count: 0,
	results: [],
	names: []
}

export const comicsReducer = (state, action) => {
	switch (action.type) {
		case SET_COMICS:
			return {
				...state,
				results: [...state.results, ...action.payload.results],
				offset: action.payload.offset,
				limit: action.payload.limit,
				total: action.payload.total,
				count: state.count + action.payload.count
			}
		case RESET_COMICS:
			return {
				...state,
				...initialState
			}
		case RESET_COMICS_DATA:
			return {
				...state,
				results: []
			}
		case SET_COMIC_NAMES:
			return {
				...state,
				names: action.payload
			}
		default:
			return state
	}
}
