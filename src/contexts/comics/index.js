/*----- Core -----*/
import React from 'react'
import PropTypes from 'prop-types'

/*----- Contexts -----*/
import {initialState, comicsReducer} from './reducer'
export const ComicsContext = React.createContext([])

const ComicsProvider = ({children}) => {
	const [comics, dispatch] = React.useReducer(comicsReducer, initialState)
	return (
		<ComicsContext.Provider value={{comics, dispatch}}>
			{children}
		</ComicsContext.Provider>
	)
}

export default ComicsProvider

ComicsProvider.propTypes = {
	children: PropTypes.node.isRequired
}
