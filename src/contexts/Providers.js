import React from 'react'
import ComicCharactersProvider from './comicCharacters/index'
import Theme from './theme/index'
import ComicDetailProvider from './comicDetail/index'
import ComicsProvider from './comics/index'
import PropTypes from 'prop-types'

const Providers = ({children}) => {
	return (
		<ComicCharactersProvider>
			<ComicsProvider>
				<ComicDetailProvider>
					<Theme>{children}</Theme>
				</ComicDetailProvider>
			</ComicsProvider>
		</ComicCharactersProvider>
	)
}

export default Providers

Providers.propTypes = {
	children: PropTypes.node.isRequired
}
