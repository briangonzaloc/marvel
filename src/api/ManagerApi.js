import axios from 'axios'

const CHARACTERS = 'https://gateway.marvel.com/v1/public/characters'
const API_KEY = 'a07a3d2938635e599a34cf4192498590'
const COMICS = 'https://gateway.marvel.com/v1/public/comics/'

const DATA_PARAMS = {
	apikey: API_KEY
}

export const getComicDetail = async comicId => {
	const result = await axios
		.get(`${COMICS}/${comicId}`, {params: DATA_PARAMS})
		.then(response => {
			return response
		})
		.catch(error => {
			return error
		})
	return result
}

export const fetchComicCharacters = async params => {
	let allParams = {...DATA_PARAMS, ...params}
	const result = await axios
		.get(`${CHARACTERS}`, {params: allParams})
		.then(response => {
			return response
		})
		.catch(error => {
			return error
		})
	return result
}

export const fetchComics = async (id, params) => {
	let allParams = {...DATA_PARAMS, ...params, limit: 10}
	const result = await axios
		.get(`${CHARACTERS}/${id}/comics`, {params: allParams})
		.then(response => {
			return response
		})
		.catch(error => {
			return error
		})
	return result
}
