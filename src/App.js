import React from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'

import Header from './components/header/index'
import GlobalStyle from './GlobalStyle'
import Providers from './contexts/Providers'
import Home from './pages/Home'

const App = () => (
	<Router>
		<Providers>
			<Header />
			<Route exact path="/" component={Home} />
			<GlobalStyle />
		</Providers>
	</Router>
)

export default App
